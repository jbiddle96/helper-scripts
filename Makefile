BIN = ./bin/
SRC = ./src/
SUBDIRS = ${BIN} ${SRC}

all: ${SUBDIRS}

${BIN}: ${SRC}

${SUBDIRS}:
	${MAKE} -C $@

clean:
	$(foreach dir, ${SUBDIRS}, ${MAKE} -C ${dir} clean;)

.PHONY: clean all ${SUBDIRS}
