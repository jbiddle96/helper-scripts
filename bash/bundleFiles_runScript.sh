#!/bin/bash
#PBS -S /bin/bash
#PBS -P e31
#PBS -V
#PBS -q normal
#PBS -l ncpus=1,mem=32gb,walltime=10:00:00
#PBS -l storage=gdata/e31,storage=scratch/e31
#PBS -l wd
#PBS -j oe

ulimit -s unlimited

tar -cvzf $out $in
# tar -cvzf /scratch/e31/jb3708/VO100Sw.tar.gz VortexOnly2_100Sweep/
