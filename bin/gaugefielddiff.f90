program GaugeFieldDiff
  use kinds
  use FileIO
  implicit none

  ! Command line variables
  character(len=5)   :: ns_c, nt_c, sigfig_c
  character(len=4)   :: field_format
  character(len=255) :: file1, file2

  ! Gauge fields
  complex(DP), dimension(:,:,:,:,:,:,:), allocatable :: U_1, U_2
  real(DP), dimension(:,:,:,:,:,:,:), allocatable :: ur,ui
  integer, dimension(:,:,:,:,:), allocatable :: Diff
  real(DP) :: uzero, beta

  ! Local vars
  integer :: sigfig
  real(DP) :: eps
  integer(long) :: var
  integer :: ix, iy, iz, it, mu, ic, jc

  ! IO vars
  integer, parameter :: logfl = 102

  ! Read command line arguments
  call get_command_argument(1, ns_c)
  call get_command_argument(2, nt_c)
  call get_command_argument(3, field_format)
  call get_command_argument(4, file1)
  call get_command_argument(5, file2)
  call get_command_argument(6, sigfig_c)

  ! Parse command line arguments
  read(ns_c,'(i)') ns
  read(nt_c,'(i)') nt
  read(sigfig_c,'(i)') sigfig

  allocate(U_1(ns,ns,ns,nt,nd,nc,nc), U_2(ns,ns,ns,nt,nd,nc,nc))
  allocate(ur(ns,ns,ns,nt,nd,nc,nc), ui(ns,ns,ns,nt,nd,nc,nc))
  allocate(Diff(ns,ns,ns,nt,nd))
  diff = 0

  eps = 10.0d0**-sigfig

  write(*,'(a,4i6)') "Lattice has dimensions: ", ns, ns, ns, nt
  write(*,'(5a,i3,a)') "Comparing ", trim(file1), "  ", trim(file2), " to ",&
       sigfig, " significant figures"

  ! Read files
  if(field_format == "cssm") then
     call ReadGaugeField_cssm(trim(file1), ur, ui, uzero, beta)
     call MakeSU3(ur, ui)
     U_1 = cmplx(ur, ui)

     call ReadGaugeField_cssm(trim(file2), ur, ui, uzero, beta)
     call MakeSU3(ur, ui)
     U_2 = cmplx(ur, ui)

  else if(field_format == "ildg") then
     call ReadGaugeField_ildg(trim(file1), ur, ui, uzero, beta)
     U_1 = cmplx(ur, ui)

     call ReadGaugeField_ildg(trim(file2), ur, ui, uzero, beta)
     U_2 = cmplx(ur, ui)

  end if

  write(*,*) "Finished reading"

  open(logfl, file="diff.log", status="replace", action="write")

  do ix=1,ns; do iy=1,ns; do iz=1,ns; do it=1,nt
     do mu=1,nd
        associate(U1 => U_1(ix,iy,iz,it,mu,:,:), U2 => U_2(ix,iy,iz,it,mu,:,:))

          if (any( abs(real(U1 - U2)) > eps .OR. abs(aimag(U1 - U2)) > eps) ) then
             diff(ix, iy, iz, it, mu) = 1
             write(logfl,'(5i3)') ix, iy, iz, it, mu
          end if

        end associate
     end do
  end do; end do; end do; end do

  var = count(diff==1)
  if(var .NE. 0) then
     write(*,*)"Gauge links differ in ", var, " locations to ", sigfig, "significant figures"
     write(logfl,*)"Gauge links differ in ", var, " locations to ", sigfig, "significant figures"
  else
     write(*,*)"Gauge fields are identical to ", sigfig, "significant figures"
     write(logfl,*)"Gauge fields are identical to ", sigfig, "significant figures"
  end if

  close(logfl)

contains

  subroutine MAKESU3(RE, IM)
    real(DP), dimension(:,:,:,:,:,:,:), intent(inout) :: RE, IM
    integer IC, ID
    real(DP), dimension(:,:,:,:), allocatable :: NORMR
    real(DP), dimension(:,:,:,:), allocatable :: NORMI

    allocate(NORMR(ns,ns,ns,nt),NORMI(ns,ns,ns,nt))
    do ID = 1, ND
       !     Firstly, normalise the first row

       NORMR = RE(:,:,:,:,ID,1,1)**2 + IM(:,:,:,:,ID,1,1)**2 &
            + RE(:,:,:,:,ID,1,2)**2 + IM(:,:,:,:,ID,1,2)**2 &
            + RE(:,:,:,:,ID,1,3)**2 + IM(:,:,:,:,ID,1,3)**2

       NORMR = dsqrt(NORMR)

       do IC = 1, NC
          RE(:,:,:,:,ID,1,IC) = RE(:,:,:,:,ID,1,IC) / NORMR
          IM(:,:,:,:,ID,1,IC) = IM(:,:,:,:,ID,1,IC) / NORMR
       enddo

       !     Now compute row2 - (row2 dot row1)row1

       NORMR = RE(:,:,:,:,ID,2,1) * RE(:,:,:,:,ID,1,1) &
            + IM(:,:,:,:,ID,2,1) * IM(:,:,:,:,ID,1,1) &
            + RE(:,:,:,:,ID,2,2) * RE(:,:,:,:,ID,1,2) &
            + IM(:,:,:,:,ID,2,2) * IM(:,:,:,:,ID,1,2) &
            + RE(:,:,:,:,ID,2,3) * RE(:,:,:,:,ID,1,3) &
            + IM(:,:,:,:,ID,2,3) * IM(:,:,:,:,ID,1,3)

       NORMI = IM(:,:,:,:,ID,2,1) * RE(:,:,:,:,ID,1,1) &
            - RE(:,:,:,:,ID,2,1) * IM(:,:,:,:,ID,1,1) &
            + IM(:,:,:,:,ID,2,2) * RE(:,:,:,:,ID,1,2) &
            - RE(:,:,:,:,ID,2,2) * IM(:,:,:,:,ID,1,2) &
            + IM(:,:,:,:,ID,2,3) * RE(:,:,:,:,ID,1,3) &
            - RE(:,:,:,:,ID,2,3) * IM(:,:,:,:,ID,1,3)

       do IC = 1, NC
          RE(:,:,:,:,ID,2,IC) = RE(:,:,:,:,ID,2,IC) &
               - (NORMR * RE(:,:,:,:,ID,1,IC) - NORMI * IM(:,:,:,:,ID,1,IC))

          IM(:,:,:,:,ID,2,IC) = IM(:,:,:,:,ID,2,IC) &
               - (NORMR * IM(:,:,:,:,ID,1,IC) &
               + NORMI * RE(:,:,:,:,ID,1,IC))
       enddo


       !     Normalise the second row

       NORMR = RE(:,:,:,:,ID,2,1)**2 + IM(:,:,:,:,ID,2,1)**2 &
            + RE(:,:,:,:,ID,2,2)**2 + IM(:,:,:,:,ID,2,2)**2 &
            + RE(:,:,:,:,ID,2,3)**2 + IM(:,:,:,:,ID,2,3)**2

       NORMR = dsqrt(NORMR)

       do IC = 1, NC
          RE(:,:,:,:,ID,2,IC) = RE(:,:,:,:,ID,2,IC) / NORMR
          IM(:,:,:,:,ID,2,IC) = IM(:,:,:,:,ID,2,IC) / NORMR
       enddo

       !     Now generate row3 = row1 cross row2

       RE(:,:,:,:,ID,3,3) = RE(:,:,:,:,ID,1,1) * RE(:,:,:,:,ID,2,2) &
            - IM(:,:,:,:,ID,1,1) * IM(:,:,:,:,ID,2,2) &
            - RE(:,:,:,:,ID,1,2) * RE(:,:,:,:,ID,2,1) &
            + IM(:,:,:,:,ID,1,2) * IM(:,:,:,:,ID,2,1)

       RE(:,:,:,:,ID,3,2) = RE(:,:,:,:,ID,1,3) * RE(:,:,:,:,ID,2,1) &
            - IM(:,:,:,:,ID,1,3) * IM(:,:,:,:,ID,2,1) &
            - RE(:,:,:,:,ID,1,1) * RE(:,:,:,:,ID,2,3) &
            + IM(:,:,:,:,ID,1,1) * IM(:,:,:,:,ID,2,3)

       RE(:,:,:,:,ID,3,1) = RE(:,:,:,:,ID,1,2) * RE(:,:,:,:,ID,2,3) &
            - IM(:,:,:,:,ID,1,2) * IM(:,:,:,:,ID,2,3) &
            - RE(:,:,:,:,ID,1,3) * RE(:,:,:,:,ID,2,2) &
            + IM(:,:,:,:,ID,1,3) * IM(:,:,:,:,ID,2,2)

       IM(:,:,:,:,ID,3,3) = - RE(:,:,:,:,ID,1,1) * IM(:,:,:,:,ID,2,2) &
            - IM(:,:,:,:,ID,1,1) * RE(:,:,:,:,ID,2,2) &
            + RE(:,:,:,:,ID,1,2) * IM(:,:,:,:,ID,2,1) &
            + IM(:,:,:,:,ID,1,2) * RE(:,:,:,:,ID,2,1)

       IM(:,:,:,:,ID,3,2) = - RE(:,:,:,:,ID,1,3) * IM(:,:,:,:,ID,2,1) &
            - IM(:,:,:,:,ID,1,3) * RE(:,:,:,:,ID,2,1) &
            + RE(:,:,:,:,ID,1,1) * IM(:,:,:,:,ID,2,3) &
            + IM(:,:,:,:,ID,1,1) * RE(:,:,:,:,ID,2,3)

       IM(:,:,:,:,ID,3,1) = - RE(:,:,:,:,ID,1,2) * IM(:,:,:,:,ID,2,3) &
            - IM(:,:,:,:,ID,1,2) * RE(:,:,:,:,ID,2,3) &
            + RE(:,:,:,:,ID,1,3) * IM(:,:,:,:,ID,2,2) &
            + IM(:,:,:,:,ID,1,3) * RE(:,:,:,:,ID,2,2)

    end do

  end subroutine MAKESU3

end program GaugeFieldDiff
