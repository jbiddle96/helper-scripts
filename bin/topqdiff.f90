program GaugeFieldDiff
  use kinds
  implicit none
  ! integer, parameter :: DP = kind(1.0d0)
  ! integer, parameter :: long = selected_int_kind(10)
  ! integer, parameter :: nd = 4
  ! integer, parameter :: nc = 3

  ! Command line variables
  character(len=5)   :: ns_c, nt_c, sigfig_c
  character(len=255) :: file1, file2

  ! Gauge fields
  real(DP), dimension(:,:,:,:), allocatable :: Q_1, Q_2
  integer, dimension(:,:,:,:), allocatable :: Diff

  ! Local vars
  integer :: sigfig
  real(DP) :: eps
  integer(long) :: var

  call get_command_argument(1, ns_c)
  call get_command_argument(2, nt_c)
  read(ns_c,'(i)') ns
  read(nt_c,'(i)') nt

  write(*,'(a,4i6)') "Lattice has dimensions: ", ns, ns, ns, nt
  allocate(Q_1(ns,ns,ns,nt), Q_2(ns,ns,ns,nt))
  allocate(Diff(ns,ns,ns,nt))

  call get_command_argument(3, file1)
  call get_command_argument(4, file2)
  ! file1 = "/short/e31/jb3708/GluonPropagator/TopQ/util/20x20x20x40.cfg00_sw01"
  ! file2 = "/short/e31/jb3708/GluonPropagator/TopQ/util/20x20x20x40.cfg01_sw01"
  call get_command_argument(5, sigfig_c)
  read(sigfig_c,'(i)') sigfig
  eps = 10.0d0**(-real(sigfig,DP))
  write(*,'(5a,i3,a)') "Comparing ", trim(file1), "  ", trim(file2), " to ",&
       sigfig, " significant figures"
!  write(*,*) "epsilon is: ",eps
  call ReadRealField(file1, Q_1)
  call ReadRealField(file2, Q_2)

  where(abs(Q_1 - Q_2) > eps)
     Diff = 1
  end where
  var = count(Diff==1)
  if(var .NE. 0) then
     write(*,'(a,i,a,i3,a)')"Topological charge densities differ in ", var, " locations to ", sigfig, "significant figures"
  else
     write(*,'(a,i3,a)')"Topological charge densities are identical to ", sigfig, "significant figures"
  end if


contains
  subroutine ReadRealField(filename, Q)
    implicit none
    character(len=*), intent(in) :: filename
    real(DP), dimension(:,:,:,:), intent(out) :: Q

    open(101,file=filename,form='unformatted',status='old',action='read')
    read(101) Q(:,:,:,:)
    close(101)

  end subroutine ReadRealField
end program GaugeFieldDiff
