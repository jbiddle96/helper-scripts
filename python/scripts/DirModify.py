#!/apps/python3/3.7.4/bin/python3
import os, sys
import subprocess
import numpy as np


def RemoveDirs(dirName):
    """
    Remove all instances of dirName below the current directory

    Inputs:
    -------
    dirName - Directory name to remove
    """
    for root, dirs, files in os.walk("."):
        if(dirs and np.any(np.array(dirs) == dirName)):
            print(bashcmd)
            bashcmd = "rm -r {}".format(dirName)
            process = subprocess.Popen(bashcmd.split(), stdout=subprocess.PIPE, cwd=root)
            output, error = process.communicate()


def AddFile(fileName, contents, dirName):
    """
    Add fileName containing contents to every directory dirName

    Inputs:
    -------
    fileName - Name of file to be created
    contents - Contents of file
    dirName - Directory name to created file in
    """
    for root, dirs, files in os.walk("."):
        if(dirs and np.any(np.array(dirs) == dirName)):
            writeDir = os.path.join(root, dirName)
            print("Writing {} in {}".format(fileName, writeDir))
            bashcmd = "touch {0}; echo {1}>{0}".format(fileName, contents)
            process = subprocess.Popen(bashcmd, shell=True,
                                       stdout=subprocess.PIPE, cwd=writeDir)
            output, error = process.communicate()


def RenameDirs(oldDirName, newDirName, dryRun=False):
    """
    Rename all instances of directory oldDirName to newDirName.

    Inputs:
    -------
    oldDirName - Directory to change
    newDirName - New directory name
    dryRun - Show changes without making modifications
    """
    for root, dirs, files in os.walk("."):
        if(dirs and np.any(np.array(dirs) == oldDirName)):
            bashcmd = "mv {} {}".format(oldDirName, newDirName)
            print("In {}".format(root))
            print(bashcmd+"\n")
            if(not dryRun):
                process = subprocess.Popen(bashcmd, shell=True,
                                           stdout=subprocess.PIPE, cwd=root)
                output, error = process.communicate()


def FindReplace(string, sub, fileExt, dryRun=False):
    """
    Find and replace all instances of string with sub
    in files with extension fileExt

    Inputs:
    -------
    string - String to replace
    sub - Replacement string
    fileExt - Extension of files to modify
    dryRun - Show changes without making modifications
    """
    for root, dirs, files in os.walk("."):
        for f in files:
            _, ext = os.path.splitext(f)
            if(ext == fileExt):
                fPath = os.path.join(root, f)
                new_file_content = ""
                counts = 0
                with open(fPath, "r") as fp:
                    for line in fp:
                        strippedLine = line.strip()
                        counts += strippedLine.count(string)
                        newLine = strippedLine.replace(string, sub)
                        new_file_content += newLine + "\n"

                if(dryRun and (counts > 0)):
                    print("Located file {}".format(fPath))
                    print("Found {} occurences".format(counts))
                elif(counts > 0):
                    print("Located file {}".format(fPath))
                    print("Found and replaced {} occurences".format(counts))
                    writeFile = open(fPath, "w")
                    writeFile.write(new_file_content)
                    writeFile.close()


RenameDirs("20x40_Gaugefield_NewMCG", "20x40_PG_NewMCG")
FindReplace("/20x40_Gaugefield_NewMCG", "/20x40_PG_NewMCG", ".sh")
print()
RenameDirs("20x40_GaugeField_NewMCG", "20x40_PG_NewMCG")
FindReplace("/20x40_GaugeField_NewMCG", "/20x40_PG_NewMCG", ".sh")
# RenameDirs("20x40_GaugeField", "20x40_PG")
# FindReplace("/20x40_GaugeField", "/20x40_PG", ".sh")
# print()
# RenameDirs("32x64_FullQCD", "32x64_B1900_Kud01378100_Ks01364000")
# FindReplace("/32x64_FullQCD", "/32x64_B1900_Kud01378100_Ks01364000", ".sh")
