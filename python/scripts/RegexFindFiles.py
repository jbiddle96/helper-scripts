import numpy as np
import re
import os, sys

path = "/g/data/e31/jb3708/data/LandauFixed/UT-MCG-VO-Rand/32x64_FullQCD/Logs"
files = []
for (dirpath, dirnames, filenames) in os.walk(path):
    files.extend(filenames)

final_iter = []
for f in files:
    f = os.path.join(path, f)
    matches = [re.findall(r'^ +\d+',line) 
               for line in open(f)]
    matches = [n[0] for n in matches if n]
    final_iter.append(matches[-1])

final_iter = [int(n) for n in final_iter]
print(final_iter)
print(np.max(final_iter))
print(np.min(final_iter))

