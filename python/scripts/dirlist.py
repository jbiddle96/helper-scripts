#!/usr/bin/env python3
import os
from fnmatch import fnmatch
import argparse


def walklevel(path, depth=1):
    """
    It works just like os.walk, but you can pass it a level parameter
    that indicates how deep the recursion will go.
    If depth is -1 (or less than 0), the full depth is walked.
    """
    # if depth is negative, just walk
    if depth < 0:
        for root, dirs, files in os.walk(path):
            yield root, dirs, files

    # path.count works because is a file has a "/" it will show up in the list
    # as a ":"
    path = path.rstrip(os.path.sep)
    num_sep = path.count(os.path.sep)
    for root, dirs, files in os.walk(path):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + depth <= num_sep_this:
            del dirs[:]


def FindPattern(path, pattern, exclusions=False):
    result = []
    for root, dirs, files in walklevel(path, depth=0):
        files.sort()
        for name in files:
            if fnmatch(name, pattern) and not exclusions:
                result.append(os.path.join(root, name))
            elif fnmatch(name, pattern) and not any(l in name
                                                    for l in exclusions):
                result.append(os.path.join(root, name))
    return result


def DropExtensions(matches):
    for i, name in enumerate(matches):
        matches[i] = os.path.splitext(name)[0]

    matches = list(set(matches))
    return matches


if __name__ == '__main__':
    # Setup argparse
    parser = argparse.ArgumentParser(
        description=
        "Create a file containing a list of filenames matching a pattern"
        " and not matching the exclusion patterns."
    )
    parser.add_argument("path", help="Path containing files to be found.")
    parser.add_argument("pattern",
                        help="Pattern to be matched. Include * for wildcard.")
    parser.add_argument(
        "exclusions",
        default=False,
        nargs="*",
        help="Substrings to be excluded from filenames.",
    )
    parser.add_argument(
        "--dropext",
        action="store_const",
        const=True,
        default=False,
        help="Drop extension "
        '(all text after and including the final "."), '
        "then merge remaining results",
    )
    parser.add_argument("-o",
                        default="dirlist.txt",
                        help="Specify output name. Defaults to dirlist.txt")
    parser.add_argument(
        "--maxfiles",
        default=None,
        type=int,
        help="Specify a maximum number of files. Defaults to no limit.",
    )

    args = parser.parse_args()
    path = args.path
    pattern = args.pattern
    exclusions = args.exclusions
    dropext = args.dropext
    output_name = args.o
    maxfiles = args.maxfiles

    matches = FindPattern(path, pattern, exclusions)
    if dropext:
        matches = DropExtensions(matches)
    matches = sorted(matches)

    if maxfiles is not None and (maxfiles < len(matches)):
        matches = matches[:maxfiles]

    otfl = os.path.join(path, output_name)
    with open(otfl, "w") as f:
        f.write(str(len(matches)) + "\n")
        for name in matches:
            f.write(name + "\n")
