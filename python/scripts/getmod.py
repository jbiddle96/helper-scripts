#!/usr/bin/env python3
import sys
from shutil import copyfile

# print"The arguments are",str(sys.argv)
#print len(sys.argv), "Arguments were passed"
if len(sys.argv) != 4:
    sys.exit('Script requires 3 arguments, you passed {}'.format(len(sys.argv)-1 ) )
# Input program file
filename = str(sys.argv[1])
# Directory to search for .mod files
cpdir = str(sys.argv[2])
# Directory to place .mod files
outdir = str(sys.argv[3])
f=open(filename)

print("Opening file:", filename)
for line in f:
    if not line.isspace():
        first_word = line.strip().split()[0].lower()
        second_word = line.strip().split()[1].lower()
        if first_word == "use":
            try:
                copyfile(cpdir+second_word+".mod", outdir+second_word+".mod")
                print("Copied {}".format(cpdir+second_word+".mod"))
            except FileNotFoundError:
                print("File {} does not exist".format(cpdir+second_word+".mod"))

            try:
                copyfile(cpdir+second_word+".f90", outdir+second_word+".f90")
                print("Copied {}".format(cpdir+second_word+".f90"))
            except FileNotFoundError:
                print("File {} does not exist".format(cpdir+second_word+".f90"))

        if first_word.lower() == "implicit":
            sys.exit("Done")
