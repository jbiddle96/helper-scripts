#!/usr/bin/env python3
import os
import sys
import fnmatch
import argparse


class SourceFile:
    def __init__(self, filename: str, src_files: list):
        """Initialise the SourceFile object

        :param filename: Filename of the source file
        :param src_files: List of other files that may be dependencies
        :returns:

        """

        self.filename = filename
        self.scan()
        self.connect_paths(src_files)
        self.target_name = self.target_name()

    def scan(self):
        """Scan this file for immediate module dependencies.

        All dependencies are stored in lowercase
        """
        self._depends = []
        with open(self.filename, 'r') as f:
            self.type = 'module'
            for line in f:
                if line.lstrip().lower().startswith("program"):
                    self.type = 'program'

                if line.lstrip().lower().startswith("use"):
                    line_list = line.split()
                    name = line_list[1].lower()
                    # Remove trailing comma in case of use <module>, only:
                    name = name.rstrip(',')
                    self._depends.append(name)


    def connect_paths(self, src_files: list):
        """
        Find the source files correpsonding to the
        identified modules from a list of source files

        """
        for name in src_files:
            basename = os.path.splitext(os.path.basename(name))[0].lower()
            if basename in self._depends:
                idx = self._depends.index(basename)
                self._depends[idx] = name

        src_files_set = set(src_files)
        dependencies_set = set(self._depends)
        found = dependencies_set.intersection(src_files_set)
        not_found = dependencies_set.difference(src_files_set)
        self.found_depends = list(found)
        self.not_found_depends = list(not_found)

    def target_name(self):
        """
        Determine the makefile target name
        """

        stub = os.path.splitext(os.path.basename(self.filename))[0]
        if self.type == 'program':
            return stub + '.x'
        else:
            return stub + '.o'

    def find_all_dependencies(self, srcs: list):
        """Function to initiate the recursive location of subdependencies

        :param srcs: List of other SourceFile objects to traverse

        """
        self.all_dependencies = []
        src_copy = srcs.copy()
        try:
            self._recursive_dependencies(self, src_copy)
        except RecursionError:
            sys.exit("ERROR: Max recursion depth was reached. "
                     "This likely indicates a circular dependency.")

        # Remove duplicates
        self.all_dependencies = list(set(self.all_dependencies))

    def _recursive_dependencies(self, this_src, srcs: list):
        """Recursively traverse the sources in srcs to identify dependencies.

        :param this_src: Current SourceFile object under consideration
        :param srcs: List of untraversed sources

        """
        # If there are no found dependencies, return
        if not this_src.found_depends:
            return

        for file in this_src.found_depends:
            src_filenames = [s.filename for s in srcs]
            if file in src_filenames:
                # If the dependency is in this_srcs dependency list
                # add it to the dependency list and remove it from the list of sources
                # then call this function on the found source.
                # Removing prevents the same dependency being considered multiple times
                self.all_dependencies.append(file)
                idx = src_filenames.index(file)
                next_src = srcs.pop(idx)
                self._recursive_dependencies(next_src, srcs)


    def output_dependencies(self, f, dir):
        """Output dependencies to file f

        :param f: Open file object with write functionality
        :param dir: Directory currently being considered. Used to generate relative paths

        """
        out_str = self.target_name + ': '
        for dep in self.all_dependencies:
            dep_name = os.path.splitext(dep)[0] + '.o'
            out_str += os.path.relpath(dep_name, dir) + ' '

        out_str += '\n'
        f.write(out_str)


def find_sources(dir):
    """Find all .f90 and .f files in a specified directory, dir.
    """
    sources = []
    for file in os.listdir(dir):
        if fnmatch.fnmatch(file, "*.f90") or fnmatch.fnmatch(file, "*.f"):
            sources.append(os.path.join(dir, file))

    return sources

# |------------------ Script execution begins here ------------------|

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Generate makefile dependencies for Fortran source files."
    )
    parser.add_argument(
        "src_dirs",
        default=["."],
        nargs="*",
        help="Directories to generate dependency files for. Defaults to the current directory.",
    )

    parser.add_argument(
        "-v",
        "--verbose",
        action='store_true',
        help="Toggle verbose output"
    )

    args = parser.parse_args()
    # Ensure src_dirs are unqiue
    src_dirs = list(set(args.src_dirs))
    verbose = args.verbose

    # Get all source files in each source directory
    src_files = []
    srcs_by_dir = {}
    for dir in src_dirs:
        files = find_sources(dir)
        files.sort()
        src_files.extend(files)
        srcs_by_dir[dir] = files

    # If no source files are found, exit
    if not src_files:
        sys.exit("No program files found, exiting.")

    srcs = []
    for file in src_files:
        srcs.append(SourceFile(file, src_files))

    for s in srcs:
        if verbose:
            print(f"Finding dependencies for {s.filename}")
        s.find_all_dependencies(srcs)

    if verbose:
        for src in srcs:
            print("Filename:             ", src.filename)
            print("Found dependencies:   ", src.all_dependencies)
            print("Missing dependencies: ", src.not_found_depends)
            print()

    for dir, filenames in srcs_by_dir.items():
        out_name = os.path.join(dir, "depends.mk")
        # Skip empty directories
        if not filenames:
            continue
        with open(out_name, 'w') as f:
            for s in srcs:
                if s.filename in filenames:
                    s.output_dependencies(f, dir)
        print("Generated: ", out_name)
