module FileIO
  use Kinds
  implicit none

contains
!
!     cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!     this subroutine reads in the link variables.
!
!     Author: Derek B. Leinweber
!  
  subroutine ReadGaugeField_cssm(filename,ur,ui,uzero,beta)
    implicit none

    !     global variables

    character(len=*)                                 :: filename
    double precision,dimension(:,:,:,:,:,:,:)        :: ur,ui
    double precision                                 :: beta,uzero
    !character(len=*),intent(in)                                  :: filename
    !double precision,dimension(nx,ny,nz,nt,nd,nc,nc),intent(inout) :: ur,ui
    !double precision,intent(out)                                 :: beta,uzero

    !     Local variables
    integer                                                 :: nfig,nxf,nyf,nzf,ntf
    double precision                                        :: lastPlaq,plaqbarAvg
    integer                                                 :: ic

    !     Execution begins
    open(101,file=filename,form='unformatted',status='old',action='read')

    read (101) nfig,beta,nxf,nyf,nzf,ntf
    !write(*,*) nfig,beta,nxf,nyf,nzf,ntf

    do ic = 1, nc-1
       read (101) ur(:,:,:,:,:,ic,:)
       read (101) ui(:,:,:,:,:,ic,:)
    end do

    read (101) lastPlaq, plaqbarAvg, uzero
    close(101)

    return

  end subroutine ReadGaugeField_cssm

  subroutine ReadGaugeField_ildg(filename,ur,ui,uzero,beta)
    implicit none
  
    character(len=*)                  :: filename
    real(DP),dimension(:,:,:,:,:,:,:) :: ur,ui
    real(DP)                          :: uzero,beta

    ! Local vars
    complex(dp), dimension(:,:,:,:,:,:,:), allocatable  :: U_lxd
    integer :: matrix_len, irecl, irec
    integer :: ic, jc, mu

    matrix_len = 16*nc*nc
    irecl = matrix_len*nd*ns*ns*ns*nt
    allocate(U_lxd(nc,nc,nd,ns,ns,ns,nt))
    
    open(101,file=filename,form='unformatted',access='direct',status='old',action='read',recl=irecl)
    irec = 1
    read(101,rec=irec) U_lxd
    close(101)
    
    ! Take transpose and rearrange indices
    do mu = 1,nd
       do jc = 1,nc; do ic = 1,nc
          ur(:,:,:,:,mu,ic,jc) = real(U_lxd(jc,ic,mu,:,:,:,:))
          ui(:,:,:,:,mu,ic,jc) = aimag(U_lxd(jc,ic,mu,:,:,:,:))
       end do; end do
    end do

    ! u0 and beta are not stored
    uzero = 1.0d0
    beta = 1.0d0
    
    deallocate(U_lxd)
    
  end subroutine ReadGaugeField_ildg
  
end module FileIO
