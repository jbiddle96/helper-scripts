module kinds
  implicit none
  integer, parameter :: long = selected_int_kind(10)

  integer, parameter :: SP = kind(1.0)
  integer, parameter :: DP = kind(1.0d0)

  integer, parameter :: DC = kind(cmplx(1.0d0, 1.0d0))
  
  real(dp), parameter :: pi = 4.0d0*atan(1.0d0)

  ! Lattice Parameters
  integer, parameter :: ND = 4
  integer, parameter :: NC = 3

  integer :: ns, nt
end module kinds
